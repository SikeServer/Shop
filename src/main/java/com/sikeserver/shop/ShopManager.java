package com.sikeserver.shop;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

import com.sikeserver.core.CoreManager;
import com.sikeserver.core.util.ConfigManager;
import com.sikeserver.economy.EconomyManager;
import com.sikeserver.shop.command.WhatsThis;
import com.sikeserver.shop.listener.*;

public class ShopManager extends JavaPlugin {
	public CoreManager core;
	public EconomyManager economy;
	private static ShopManager plugin;
	
	private ConfigManager itemList;

	@Override
	public void onEnable() {
		plugin = this;
		core = CoreManager.getPlugin();
		economy = EconomyManager.getPlugin();
		
		try {
			itemList = new ConfigManager("items.yml", core);
		} catch (Exception e) {
			getLogger().warning("Error occured while loading config file!");
			e.printStackTrace();
		}
		
		PluginManager manager = Bukkit.getServer().getPluginManager();
		manager.registerEvents(new ChestClickListener(), this);
		manager.registerEvents(new SignCreateListener(), this);
		manager.registerEvents(new SignClickListener(), this);
		manager.registerEvents(new ShopBreakListener(), this);
		manager.registerEvents(new ChestPlaceListener(), this);
		
		getCommand("whatsthis").setExecutor(new WhatsThis());
	}
	
	public UUID getOwner(Location l) throws SQLException {
		try (Statement stmt = core.sql.getStatement();
			 ResultSet rs = stmt.executeQuery(
				"SELECT * FROM " + CoreManager.shopTable
				+ " WHERE x = " + l.getBlockX()
				+ " AND y = " + l.getBlockY()
				+ " AND z = " + l.getBlockZ()
			 )
		) {
			if (rs.next()) { // 店が存在するなら
				UUID u = UUID.fromString(rs.getString("owner"));
				return u;
			} else {
				return null;
			}
		}
	}
	
	/**
	 * Enum型のアイテム名をゲーム上で表示されるアイテム名に変換します。
	 * 
	 * @author Siketyan
	 * 
	 * @param item "Material" Enumクラスに存在するアイテム名。
	 * 
	 * @return Config（items.yml）に記述されているアイテム名。
	 */
	public String getItemName(Material item) {
		if (!itemList.contains(item.toString())) return item.toString();
		return itemList.getString(item.toString());
	}
	
	public Location getGirth(Location l) {
		if (l.subtract(1.0, 0.0, 0.0).getBlock().getType().equals(Material.CHEST)) return l.subtract(1.0, 0.0, 0.0);
		if (l.subtract(-2.0, 0.0, 0.0).getBlock().getType().equals(Material.CHEST)) return l.subtract(-2.0, 0.0, 0.0);
		if (l.subtract(0.0, 0.0, 1.0).getBlock().getType().equals(Material.CHEST)) return l.subtract(0.0, 0.0, 1.0);
		if (l.subtract(0.0, 0.0, -1.0).getBlock().getType().equals(Material.CHEST)) return l.subtract(0.0, 0.0, -1.0);
		
		return null;
	}

	public static ShopManager getPlugin() {
		return plugin;
	}
}
