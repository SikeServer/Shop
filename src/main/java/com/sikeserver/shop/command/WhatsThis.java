package com.sikeserver.shop.command;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.sikeserver.core.CoreManager;
import com.sikeserver.shop.ShopManager;

public class WhatsThis implements CommandExecutor {
	private CoreManager core;
	private ShopManager plugin;
	
	public WhatsThis() {
		plugin = ShopManager.getPlugin();
		core = plugin.core;
	}
	
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		// コンソールからの実行でないかチェック
		if (!(sender instanceof Player)) {
			sender.sendMessage(core.getMessage("Error.Common.Console"));
			return true;
		}
		
		Player p = (Player)sender;
		Material m = p.getItemOnCursor().getType();
		p.sendMessage(
			"" + ChatColor.AQUA + ChatColor.BOLD
				+ "[i]" + ChatColor.WHITE + plugin.getItemName(m)
				+ " : " + ChatColor.YELLOW + ChatColor.BOLD + m
		);
		
		return true;
	}
}
