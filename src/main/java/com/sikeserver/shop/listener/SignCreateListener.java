package com.sikeserver.shop.listener;

import java.sql.SQLException;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.Chest;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.SignChangeEvent;
import org.bukkit.inventory.ItemStack;

import com.sikeserver.core.CoreManager;
import com.sikeserver.core.util.SQLManager;
import com.sikeserver.shop.ShopManager;

public class SignCreateListener implements Listener {
	private ShopManager plugin;
	private CoreManager core;
	private SQLManager sql;
	
	public SignCreateListener() {
		plugin = ShopManager.getPlugin();
		core = plugin.core;
		sql = core.sql;
	}
	@EventHandler
	public void onSignChange(SignChangeEvent event) {
		Player p = event.getPlayer();
		
		// 看板がShop用かチェック
		if (!event.getLine(0).equals("{Shop}")) return;
		
		// 下にチェストがあるかチェック
		Block under = event.getBlock().getLocation().subtract(0, 1.0, 0).getBlock();
		if (under == null || !(under.getState() instanceof Chest)) {
			p.sendMessage(core.getMessage("Error.Shop.NoChest"));
			dropSign(event.getBlock().getLocation(), p);
			return;
		}
		
		Location l = plugin.getGirth(under.getLocation());
		if (l != null && l.getBlock().getState() instanceof Chest) {
			p.sendMessage(core.getMessage("Error.Shop.LargeChest"));
			dropSign(event.getBlock().getLocation(), p);
			return;
		}
		
		// 指定されたアイテムが存在するかチェック
		Material m = Material.getMaterial(event.getLine(1));
		if (m == null) {
			p.sendMessage(core.getMessage("Error.Shop.UnknownItem"));
			dropSign(event.getBlock().getLocation(), p);
			return;
		}
		
		// 一口が最大スタックを超えていないかのチェックおよび変数への代入
		int amount;
		try {
			amount = Integer.parseInt(event.getLine(2));
			if (amount > m.getMaxStackSize()) {
				p.sendMessage(core.getMessage("Error.Shop.OverStack"));
				dropSign(event.getBlock().getLocation(), p);
				return;
			}
		} catch (NumberFormatException e) {
			p.sendMessage(core.getMessage("Error.Shop.FormatAmount"));
			dropSign(event.getBlock().getLocation(), p);
			return;
		}
		
		// 価格の形式チェック
		try {
			Long.parseLong(event.getLine(3));
		} catch (NumberFormatException e) {
			p.sendMessage(core.getMessage("Error.Shop.FormatPrice"));
			dropSign(event.getBlock().getLocation(), p);
			return;
		}
		
		// 登録
		try {
			sql.executeUpdate(
				"INSERT INTO " + CoreManager.shopTable + "(x, y, z, owner) VALUES("
					+ event.getBlock().getX() + ", "
					+ event.getBlock().getY() + ", "
					+ event.getBlock().getZ() + ", \""
					+ event.getPlayer().getUniqueId().toString() + "\")"
			);
		} catch (SQLException e) {
			p.sendMessage(core.getMessage("Error.Common.SQL"));
			plugin.getLogger().warning("Oh,no... Error occurred in SQL query!");
			dropSign(event.getBlock().getLocation(), p);
			e.printStackTrace();
			return;
		}
		
		p.sendMessage(core.getMessage("Message.Shop.Registed"));
		plugin.getLogger().info(
			p.getName() + " created a shop at"
				+ event.getBlock().getX() + ", "
				+ event.getBlock().getY() + ", "
				+ event.getBlock().getZ()
		);
	}
	
	private void dropSign(Location l, Player p) {
		l.getBlock().setType(Material.AIR);
		p.getWorld().dropItemNaturally(p.getLocation(), new ItemStack(Material.SIGN, 1));
	}
}
