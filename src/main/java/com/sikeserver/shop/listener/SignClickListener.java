package com.sikeserver.shop.listener;

import java.sql.SQLException;
import java.util.UUID;

import org.bukkit.Material;
import org.bukkit.OfflinePlayer;
import org.bukkit.block.Block;
import org.bukkit.block.Chest;
import org.bukkit.block.Sign;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

import com.sikeserver.core.CoreManager;
import com.sikeserver.economy.EconomyManager;
import com.sikeserver.shop.ShopManager;

public class SignClickListener implements Listener {
	private ShopManager plugin;
	private CoreManager core;
	private EconomyManager economy;
	
	public SignClickListener() {
		plugin = ShopManager.getPlugin();
		economy = EconomyManager.getPlugin();
		core = plugin.core;
	}
	
	@EventHandler
	public void onPlayerInteract(PlayerInteractEvent event) {
		Player p = event.getPlayer();
		
		// クリックされたものが看板かチェック
		Block b = event.getClickedBlock();
		if (event.getAction() != Action.RIGHT_CLICK_BLOCK) return;
		if (b == null || !(b.getState() instanceof Sign)) return;
		
		// 看板がShop用かチェック
		Sign s = (Sign)b.getState();
		if (!s.getLine(0).equals("{Shop}")) return;
		
		// 各種情報取得
		UUID u;
		Long price;
		Block under = b.getLocation().subtract(0, 1.0, 0).getBlock();
		Chest c = (Chest)under.getState();
		String item = s.getLine(1);
		int amount = Integer.parseInt(s.getLine(2));
		
		try {
			u = plugin.getOwner(b.getLocation());
			price = Long.parseLong(s.getLine(3));

			// 店主と顧客が同じでないか
            if (u.toString().equals(p.getUniqueId().toString())) {
				p.sendMessage(core.getMessage("Error.Shop.Own"));
				return;
			}

			// 店主と顧客の所持金確認
			Long myMoney = economy.getMoney(p.getUniqueId());
			Long ownerMoney = economy.getMoney(u);
			
			if (price < 0) { // 「売る」なら
				// 商品を持っているか
				if (!p.getInventory().contains(Material.getMaterial(item), amount)) {
					p.sendMessage(core.getMessage("Error.Shop.NotEnoughItemToSell"));
					return;
				}
				
				// 店主のお金が足りるか
				if (ownerMoney < price) {
					p.sendMessage(core.getMessage("Error.Shop.NotEnoughMoneyToSell"));
					OfflinePlayer owner = plugin.getServer().getOfflinePlayer(u);
					if (owner.isOnline()) {
						owner.getPlayer().sendMessage(
							core.getMessage("Message.Shop.OwnerMoney")
								.replaceAll("%player%", p.getName())
								.replaceAll("%item%", Material.getMaterial(item).toString())
								.replaceAll("%amount%", String.valueOf(amount))
								.replaceAll("%price%", String.valueOf(price))
						);
					}
					return;
				}
				
				// チェストに空きがあるか
				if (c.getBlockInventory().firstEmpty() < 0) {
					p.sendMessage(core.getMessage("Error.Shop.ChestInventoryFull"));
					return;
				}
				
				// 決済
				economy.setMoney(p.getUniqueId(), myMoney - price);
				economy.setMoney(u, ownerMoney + price);
				
				// プレイヤーにアイテムを渡す
				p.getInventory().removeItem(new ItemStack(Material.getMaterial(item), amount));
				c.getBlockInventory().addItem(new ItemStack(Material.getMaterial(item), amount));
				
				// 完了通知
				p.sendMessage(
					core.getMessage("Message.Shop.Selled")
						.replaceAll("%owner%", core.getName(u))
						.replaceAll("%item%", plugin.getItemName(Material.getMaterial(item)))
						.replaceAll("%amount%", String.valueOf(amount))
						.replaceAll("%price%", String.valueOf(price))
				);
				
				// 店主への通知
				OfflinePlayer owner = plugin.getServer().getOfflinePlayer(u);
				if (owner.isOnline()) {
					owner.getPlayer().sendMessage(
						core.getMessage("Message.Shop.OwnerNoticeSell")
							.replaceAll("%player%", p.getName())
							.replaceAll("%item%", plugin.getItemName(Material.getMaterial(item)))
							.replaceAll("%amount%", String.valueOf(amount))
							.replaceAll("%price%", String.valueOf(price))
					);
				}
				
				plugin.getLogger().info(
					p.getName() + " selled "
						+ amount + " "
						+ plugin.getItemName(Material.getMaterial(item))
						+ "(s) to " + core.getName(u) + "'s shop at "
						+ b.getX() + ", "
						+ b.getY() + ", "
						+ b.getZ()
				);
			} else { // 「買う」なら
				// 在庫があるか
				if (!c.getBlockInventory().contains(Material.getMaterial(item), amount)) {
					p.sendMessage(core.getMessage("Error.Shop.NotEnoughItem"));
					OfflinePlayer owner = plugin.getServer().getOfflinePlayer(u);
					if (owner.isOnline()) {
						owner.getPlayer().sendMessage(
							core.getMessage("Message.Shop.OwnerStock")
								.replaceAll("%item%", Material.getMaterial(item).toString())
								.replaceAll("%amount%", String.valueOf(amount))
								.replaceAll("%price%", String.valueOf(price))
						);
					}
					return;
				}
				
				// お金が足りるか
				if (myMoney < price) {
					p.sendMessage(core.getMessage("Error.Economy.NotEnoughMoney"));
					return;
				}
				
				// インベントリが満杯でないかチェック
				if (p.getInventory().firstEmpty() < 0) {
					p.sendMessage(core.getMessage("Error.Shop.InventoryFull"));
					return;
				}
				
				// 決済
				economy.setMoney(p.getUniqueId(), myMoney - price);
				economy.setMoney(u, ownerMoney + price);
				
				// プレイヤーにアイテムを渡す
				c.getBlockInventory().removeItem(new ItemStack(Material.getMaterial(item), amount));
				p.getInventory().addItem(new ItemStack(Material.getMaterial(item), amount));
				
				// 完了通知
				p.sendMessage(
					core.getMessage("Message.Shop.Buyed")
						.replaceAll("%owner%", core.getName(u))
						.replaceAll("%item%", plugin.getItemName(Material.getMaterial(item)))
						.replaceAll("%amount%", String.valueOf(amount))
						.replaceAll("%price%", String.valueOf(price))
				);
				
				// 店主への通知
				OfflinePlayer owner = plugin.getServer().getOfflinePlayer(u);
				if (owner.isOnline()) {
					owner.getPlayer().sendMessage(
						core.getMessage("Message.Shop.OwnerNotice")
							.replaceAll("%player%", p.getName())
							.replaceAll("%item%", plugin.getItemName(Material.getMaterial(item)))
							.replaceAll("%amount%", String.valueOf(amount))
							.replaceAll("%price%", String.valueOf(price))
					);
				}
				
				plugin.getLogger().info(
					p.getName() + " bought "
						+ amount + " "
						+ plugin.getItemName(Material.getMaterial(item))
						+ "(s) from " + core.getName(u) + "'s shop at "
						+ b.getX() + ", "
						+ b.getY() + ", "
						+ b.getZ()
				);
			}
		} catch (SQLException e) { // SQLエラー処理
			p.sendMessage(core.getMessage("Error.Common.SQL"));
			plugin.getLogger().warning("Oh,no... Error occurred in SQL query!");
			e.printStackTrace();
		}
	}
}
