package com.sikeserver.shop.listener;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.block.BlockState;
import org.bukkit.block.Chest;
import org.bukkit.block.Sign;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;

import com.sikeserver.core.CoreManager;
import com.sikeserver.core.util.SQLManager;
import com.sikeserver.shop.ShopManager;

public class ChestClickListener implements Listener {
	private ShopManager plugin;
	private CoreManager core;
	private SQLManager sql;
	
	public ChestClickListener() {
		plugin = ShopManager.getPlugin();
		core = plugin.core;
		sql = core.sql;
	}
	
	@EventHandler
	public void onPlayerInteract(PlayerInteractEvent event) {
		Block b = event.getClickedBlock();
		if (event.getAction() != Action.RIGHT_CLICK_BLOCK) return;
		if (b == null || !(b.getState() instanceof Chest)) return;
		
		BlockState state = b.getLocation().subtract(0, -1.0, 0).getBlock().getState();
		if (!(state instanceof Sign)) return;
		Sign s = (Sign)state;
		if (!s.getLine(0).equals("{Shop}")) return;
		
		Location l = b.getLocation();
		Player p = event.getPlayer();
		try(Statement stmt = sql.getStatement();
			ResultSet rs = stmt.executeQuery(
				"SELECT * FROM " + CoreManager.shopTable
					+ " WHERE x = " + l.getBlockX()
					+ " AND y = " + l.getBlockY()
					+ " AND z = " + l.getBlockZ()
			))
		{
			if (!rs.next()) return;
			if (!rs.getString("owner").equals(p.getUniqueId().toString())) {
				p.sendMessage(core.getMessage("Error.Shop.NotOwnerChest"));
				event.setCancelled(true);
				return;
			}
		} catch (SQLException e) {
			p.sendMessage(core.getMessage("Error.Common.SQL"));
			plugin.getLogger().warning("Oh,no... Error occurred in SQL query!");
			e.printStackTrace();
			event.setCancelled(true);
			return;
		}
	}
}
