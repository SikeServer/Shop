package com.sikeserver.shop.listener;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.BlockState;
import org.bukkit.block.Sign;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockPlaceEvent;

import com.sikeserver.core.CoreManager;
import com.sikeserver.shop.ShopManager;

public class ChestPlaceListener implements Listener {
	private ShopManager plugin;
	private CoreManager core;
	
	public ChestPlaceListener() {
		plugin = ShopManager.getPlugin();
		core = plugin.core;
	}
	
	@EventHandler
	public void onBlockPlace(BlockPlaceEvent event) {
		if (event.getBlockPlaced().getType() != Material.CHEST) return;
		
		Location l = plugin.getGirth(event.getBlock().getLocation());
		if (l == null) return;
		
		BlockState state = l.subtract(0.0, 1.0, 0.0).getBlock().getState();
		if (!(state instanceof Sign) || !((Sign)state).getLine(0).equals("{Shop}")) return;
		
		event.getPlayer().sendMessage(core.getMessage("Error.Shop.LargeChest"));
		event.setCancelled(true);
	}
}