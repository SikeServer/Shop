package com.sikeserver.shop.listener;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.BlockState;
import org.bukkit.block.Chest;
import org.bukkit.block.Sign;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.inventory.ItemStack;

import com.sikeserver.core.CoreManager;
import com.sikeserver.core.util.SQLManager;
import com.sikeserver.shop.ShopManager;

public class ShopBreakListener implements Listener {
	private ShopManager plugin;
	private CoreManager core;
	private SQLManager sql;
	
	public ShopBreakListener() {
		plugin = ShopManager.getPlugin();
		core = plugin.core;
		sql = core.sql;
	}
	
	@EventHandler(priority = EventPriority.HIGHEST)
	public void onBlockBreak(BlockBreakEvent event) {
		BlockState state = event.getBlock().getState();
		
		try {			
			if (state instanceof Sign) {
				Sign s = (Sign)state;
				if (!s.getLine(0).equals("{Shop}")) return;
				event.setCancelled(
					onSignBreak(event.getBlock().getLocation(), event.getPlayer())
				);
			}
			if (state instanceof Chest) {
				BlockState upper = event.getBlock().getLocation()
										.subtract(0, -1.0, 0)
										.getBlock()
										.getState();
				if (!(upper instanceof Sign)) return;
								
				Sign s = (Sign)upper;
				if (!s.getLine(0).equals("{Shop}")) return;
				
				event.setCancelled(
					onChestBreak(upper.getLocation().subtract(0, -1.0, 0), event.getPlayer())
				);
			}
		} catch (SQLException e) {
			event.getPlayer().sendMessage(core.getMessage("Error.Common.SQL"));
			plugin.getLogger().warning("Oh,no... Error occurred in SQL query!");
			e.printStackTrace();
			return;
		}
	}
	
	/**
	 * 看板が壊されたときのショップ削除処理。
	 * ショップでない場合はスキップされます。
	 * 
	 * @author Siketyan
	 * 
	 * @param l <b>看板の</b>場所
	 * @param p イベントを起こしたプレイヤー
	 * 
	 * @return イベントをキャンセルするかどうか。
	 * 
	 * @throws SQLException
	 */
	private boolean onSignBreak(Location l, Player p) throws SQLException {
		try(Statement stmt = sql.getStatement();
			ResultSet rs = stmt.executeQuery(
				"SELECT * FROM " + CoreManager.shopTable
					+ " WHERE x = " + l.getBlockX()
					+ " AND y = " + l.getBlockY()
					+ " AND z = " + l.getBlockZ()
			))
		{
			plugin.getLogger().info("SELECT * FROM " + CoreManager.shopTable
					+ " WHERE x = " + l.getBlockX()
					+ " AND y = " + l.getBlockY()
					+ " AND z = " + l.getBlockZ());
			if (!rs.next()) return false;			
			if (!rs.getString("owner").equals(p.getUniqueId().toString())) {
				p.sendMessage(core.getMessage("Error.Shop.NotOwner"));
				return true;
			}
			
			sql.executeUpdate(
				"DELETE FROM " + CoreManager.shopTable
					+ " WHERE x = " + l.getBlockX()
					+ " AND y = " + l.getBlockY()
					+ " AND z = " + l.getBlockZ()
			);
			
			p.sendMessage(core.getMessage("Message.Shop.Deleted"));
			plugin.getLogger().info(
				p.getName() + " deleted a shop at "
					+ l.getBlockX() + ", "
					+ l.getBlockY() + ", "
					+ l.getBlockZ()
			);
			return false;
		}
	}
	
	private boolean onChestBreak(Location l, Player p) throws SQLException {
		if(onSignBreak(l.subtract(0, 1.0, 0), p)) return true;
		
		l.getBlock().setType(Material.AIR);
		p.getWorld().dropItemNaturally(p.getLocation(), new ItemStack(Material.SIGN, 1));
		
		return false;
	}
}
